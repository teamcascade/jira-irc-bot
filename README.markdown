# JIRABot
![JIRABot][1]

What is JIRABot?
------------------------
JIRA IRC Bot is a plugin for the Atlassian JIRA issues management system. The plugin allows development teams to have a presence in IRC chat rooms to help support their users. The IRC Bot will connect from your JIRA instance to an IRC network and allow chat users to query projects and issues.
The JIRA administrators can configure the IRC Bot from inside their JIRA instance administration section including multiple chat-rooms with each its own subset of projects associated with it.

Core Ideas:
-----------
- _CONCEPTS
    + A lot of development teams provide support and/or use IRC channels to communicate
    + A lot of development teams use JIRA and other Atlassian products durng their development phases
    + Many development teams have developped their own IRC Bot over the years
    + IRC Bot accessing JIRA from outside via REST APIs are limited to the REST APIs
    + Integrating an IRC Bot as a plugin for JIRA removes  the need for teams to create their own
    + Integrating an IRC Bot as a plugin for JIRA removes the limits of the REST API
    + A GUI to manage the IRC Bot inside JIRA provides a constant look and feel and ease of use

- _ADMINISTRATION_
    + Standard JIRA Adminstration user interface
    + Create/manage multiple chat channels (chat room)
    + Associate projects to chat rooms
    + Manage Operators, Half-operators and +Voiced user and permissions
    + Manage the types of Issue transitions to report in the chat room per project
    + Gather usage data
    + Provide chat logs

- _BOT COMMANDS_
    + Standard IRC Commands:
        * .kick
        * .ban
        * etc

    + JIRA IRC Commands:
        * .critical:    List all critical open issues for projects (or single with project key)
        * .projects:    List all associated projects for the chat room (by project key)
        * .jql          Provide Jira Query Language access to the user


Who is TeamCascade?
-------------------

TeamCascade is the team behind the different projects originating from TeamCascade, we work together on our plugins, sometimes as a team, sometimes as one developer with some helping hands.

| ![Don Redhorse][10]   | ![Duck][11]   |
| Don Redhorse  | Dukmeister    |

Visit our [website][2].
Track and submit issues and bugs on our [issue tracker][3].

Source
------
The latest and greatest source can be found on [BitBucket][4]
Download the latest builds from [Bamboo][5].
You can get maven artifacts from [Artifactory][6].
And you will find the documentation in our [Doxygen Repo][7]

License
-------
JIRABot is licensed under [TeamCascade Public License v1][8], but with a provision that files are released under the MIT license 180 days after they are published. Please see the `LICENSE.txt` file for details.

Compiling
---------
JIRABot uses Maven to handle its dependencies.

* Install [Maven 2 or 3](http://maven.apache.org/download.html)
* Checkout this repo and run: `mvn clean package install`

Coding Standards
----------------------------------
* If / for / while / switch statement: if (conditions && stuff) {
* Method: public void method(Object paramObject) {
* No Spaces, Tab is preferred!
* No trailing whitespace
* Mid-statement newlines at a 200 column limit
* camelCase, no under_scores except constants
* Constants in full caps with underscores
* Keep the same formatting style everywhere
* You can use the TeamCascadeCodeScheme.xml to implement our coding formatting style in most IDE

Pull Request Standards
----------------------------------
* Sign-off on all commits!
* Finished product must compile successfully with `mvn`!
* No merges should be included in pull requests unless the pull request's purpose is a merge.
* Number of commits in a pull request should be kept to *one commit* and all additional commits must be *squashed*. Pull requests that make multiple changes should have one commit per change.
* Pull requests must include any applicable license headers. (These are generated when running `mvn clean`)

[1]: http://www.teamcascade.org/plugins/servlet/zenfoundation/zenservlet/designs/brands/tc-alpha1/images/logo1.png
[2]: http://www.teamcascade.org
[3]: http://issues.teamcascade.org
[4]: https://bitbucket.org/teamcascade/${project.name}
[5]: http://builds.teamcascade.org
[6]: http://artifacts.teamcascade.org
[7]: http://docs.teamcascade.org
[8]: http://www.teamcascade.org/display/AboutUs/TeamCascade+Public+License
[10]: http://www.gravatar.com/avatar/5715022800b638db4951afe80841b314.png?size=55
[11]: https://dl.dropbox.com/u/userid/file.png

