
AJS.toInit(function() {
  var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");

  function populateForm() {
     // Get the data from REST services (JSON)
     AJS.$.ajax({
        url: baseUrl + "/rest/jirabot/1.0/",
        dataType: "json",
        success: function(botdata) {
          AJS.$("#name").attr("value", botdata.botname);
        }
      });
    }


  function updateBot() {
      AJS.$.ajax({
        url: baseUrl + "/rest/jirabot/1.0/",
        type: "PUT",
        contentType: "application/json",
        data: '{ "botname": "' + AJS.$("#name").attr("value") +  '"}',
        processData: false
      });


    }

  function connectBot() {

  }

  populateForm();

  AJS.$("#admin").submit(function(e) {
      e.preventDefault();
      updateBot();
    });

    AJS.$("#connect").submit(function(e) {
      e.preventDefault();
      connectBot();
    });
});