/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.gui;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import java.io.IOException;
import java.util.ArrayList;

import org.pircbotx.exception.IrcException;
import org.pircbotx.exception.NickAlreadyInUseException;

import org.teamcascade.plugins.jirabot.service.AoDataService;



 
public class AdminServlet extends HttpServlet
{
	  private final UserManager userManager;
	  private final LoginUriProvider loginUriProvider;
	  private final TemplateRenderer renderer;
      private final AoDataService botData;
      private final ActiveObjects ao;
	 
	  public AdminServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer renderer,ActiveObjects ao)
	  {
	    this.userManager = userManager;
	    this.loginUriProvider = loginUriProvider;
	    this.renderer = renderer;
        this.ao = ao;
        this.botData = new AoDataService(this.ao);
	  }
	 
	  @Override
	  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	  {
	    String username = userManager.getRemoteUsername(request);
	    if (username == null || !userManager.isSystemAdmin(username))
	    {
	      redirectToLogin(request, response);
	      return;
	    }
          response.setContentType("text/html;charset=utf-8");
          renderer.render("admin.vm", response.getWriter());
	  }
	  private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
	  {
	    response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
	  }
	    
	  private URI getUri(HttpServletRequest request)
	  {
	    StringBuffer builder = request.getRequestURL();
	    if (request.getQueryString() != null)
	    {
	      builder.append("?");
	      builder.append(request.getQueryString());
	    }
	    return URI.create(builder.toString());
	  }


      public void connectBot() {

          final ArrayList<String> channels = botData.getChannelsList();
          final String nickName = botData.getNickName();
          final String serverURL =  botData.getServerUrl();

          // connect to IRC Network
          org.teamcascade.plugins.jirabot.JIRABotImpl.getIRCBot().setName(nickName);
          try {
                org.teamcascade.plugins.jirabot.JIRABotImpl.getIRCBot().connect(serverURL);
          } catch (IOException e) {

          } catch (IrcException e) {

          }
          // Join each channel associated with the Bot Instance
          for(int i=0; i<channels.size(); i++){
              org.teamcascade.plugins.jirabot.JIRABotImpl.getIRCBot().joinChannel(channels.get(i));
          }

        }
      public void discoonnectBot() {
          org.teamcascade.plugins.jirabot.JIRABotImpl.getIRCBot().disconnect();

      }
}