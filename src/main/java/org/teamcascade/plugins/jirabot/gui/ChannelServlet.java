/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.gui;

import com.atlassian.activeobjects.external.ActiveObjects;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
 
import static com.google.common.base.Preconditions.*;
 
public class ChannelServlet extends HttpServlet {

	private final ActiveObjects ao;
	 
    public ChannelServlet(ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }	
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        res.getWriter().write("Channel servlet, doGet");
        res.getWriter().close();
    }
 
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        res.getWriter().write("Channel servlet, doPost");
        res.getWriter().close();
    }
}
