/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.rest.resources;


import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.teamcascade.plugins.jirabot.service.AoDataService;
import org.pircbotx.PircBotX;


@Path("/")
public class BotResource {
    private final UserManager userManager;
    private final TransactionTemplate transactionTemplate;
    private final AoDataService botData ;

    public BotResource(UserManager userManager, TransactionTemplate transactionTemplate, AoDataService dataService)
    {
        this.userManager = userManager;
        this.transactionTemplate = transactionTemplate;
        this.botData = dataService;

    }

    // Serialization
    // Note to self: make sure any inner classes are declared as static for Jackson so there is
    // no hidden constructor that messes up Jackson type de-serialization (took forever to figure out)
    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    public static final class BotRestAPI
    {
        @XmlElement private String botname;


        public String getBotName()
        {
            return this.botname;
        }

        public void setBotName(String name)
        {
            this.botname = name;
        }


    }

    // GET VERB

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest request)
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username))
        {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        return Response.ok(transactionTemplate.execute(new TransactionCallback()
        {
            public Object doInTransaction()
            {
                BotRestAPI data = new BotRestAPI();
                data.setBotName(botData.getNickName());

                return data;
            }
        })).build();
    }

    // PUT VERB
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(final BotRestAPI data, @Context HttpServletRequest request)
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username))
        {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        transactionTemplate.execute(new TransactionCallback() // AO Updates need to be done in a transaction
        {
            @Override
            public Void doInTransaction()
            {
                // Save the new data
                botData.setNickName(data.getBotName());
                botData.getBot().save();

                // Update IRC Bot with new information
                PircBotX IRCBot = org.teamcascade.plugins.jirabot.JIRABotImpl.getIRCBot();

                IRCBot.changeNick(botData.getNickName());

                return null;
            }
        });

        return Response.noContent().build();
    }


}

