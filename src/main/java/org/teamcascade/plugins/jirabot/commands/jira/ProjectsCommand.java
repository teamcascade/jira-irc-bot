/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.commands.jira;

import com.atlassian.jira.project.Project;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.teamcascade.plugins.jirabot.service.AoDataService;
import org.teamcascade.plugins.jirabot.service.JiraService;

import java.util.ArrayList;

public class ProjectsCommand extends ListenerAdapter  {
    private AoDataService botData ;
    private JiraService restData;

    public ProjectsCommand(AoDataService data, JiraService rest) {
        this.botData = data;
        this.restData = rest;
    }

    public void onMessage(MessageEvent event) throws Exception {

        if (event.getMessage().equals("?projects")) {
            String channelName = event.getChannel().getName();
            event.getChannel().getBot().sendMessage(channelName,"Tracked projects: " );
            ArrayList<String> tracked = botData.getProjectsListByChannel(event.getChannel().getName());
            for (int i = 0; i < tracked.size(); i++) {
                String projectkey = tracked.get(i);
                Project project = restData.getProjectByKey(projectkey);

                event.getChannel().getBot().sendMessage(channelName,projectkey+" / "+project.getName());
                // TODO: use RestService to get project fields
            }



        }
    }


}