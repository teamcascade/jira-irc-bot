/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.commands.jira;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.user.util.UserManager;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.teamcascade.plugins.jirabot.service.AoDataService;


import org.teamcascade.plugins.jirabot.service.JiraService;

import java.util.List;

public class JQLCommand  extends ListenerAdapter {
    private AoDataService botData ;
    private JiraService jiraData;
    UserManager userManager = ComponentAccessor.getComponent(UserManager.class);

    public JQLCommand(AoDataService data, JiraService jiraService) {
        this.botData = data;
        this.jiraData = jiraService;
    }

    @Override
    public void onMessage(MessageEvent event) throws SearchException {

        if (event.getMessage().startsWith("?jql")) {
            String channelName = event.getChannel().getName();
            User user = userManager.getUser("jirabot");
            String jqlQuery = event.getMessage().substring(5);
            SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
            SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlQuery);
            if (parseResult.isValid()){
                Boolean found = false;
                List<Issue> issues =  jiraData.jqlQuery(user, jqlQuery);
                if (issues.size()>0) {
                    event.getChannel().getBot().sendMessage(channelName,"JQL Query: Found "+issues.size()+" matching issues :");
                    found = true;
                    for (int j=0; j<issues.size(); j++) {
                        Issue issue = issues.get(j);
                        IssueLink path =  ComponentAccessor.getIssueLinkManager().getIssueLink(issue.getId());
                        //String url = jiraHelper.getRequest().getRequestURL().toString();
                        event.getChannel().getBot().sendMessage(channelName,"    "+issue.getKey()+" - "+issue.getSummary()+" ("+")");
                    }
                }  else
                {
                    event.getChannel().getBot().sendMessage(channelName,"JQL Query: No matching issues found.");
                }

            } else {
                event.getChannel().getBot().sendMessage(channelName,"JQL Query: Syntax error in Query.");
            }
        }

    }

}