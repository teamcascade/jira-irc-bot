package org.teamcascade.plugins.jirabot.commands.jira;

import com.atlassian.jira.project.Project;
import org.pircbotx.hooks.ListenerAdapter;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.util.UserManager;
import org.pircbotx.hooks.events.MessageEvent;
import org.teamcascade.plugins.jirabot.service.AoDataService;
import org.teamcascade.plugins.jirabot.service.JiraService;


public class IssueCommand extends ListenerAdapter {

    private AoDataService botData ;
    private JiraService jiraData;
    UserManager userManager = ComponentAccessor.getComponent(UserManager.class);

    public IssueCommand(AoDataService data, JiraService jiraService) {
        this.botData = data;
        this.jiraData = jiraService;
    }

    public void onMessage(MessageEvent event) throws Exception {

        if (event.getMessage().startsWith("?issue")) {
            String channelName = event.getChannel().getName();
            String message = event.getMessage();
            if (message.length()>8) {
                String issuekey = message.substring(7);
                User user = userManager.getUser("jirabot");
                Issue issue =  jiraData.getIssueByKey(issuekey);
                if (issue != null) {
                    Project project = issue.getProjectObject();
                    event.getChannel().getBot().sendMessage(channelName,"Project "+project.getKey()+" - "+project.getName());
                    event.getChannel().getBot().sendMessage(channelName,"Issue "+issuekey+" - "+issue.getSummary() );
                    event.getChannel().getBot().sendMessage(channelName,"Type: "+issue.getIssueTypeObject().getName() );
                    event.getChannel().getBot().sendMessage(channelName,"Priority: "+issue.getPriorityObject().getName() );
                    event.getChannel().getBot().sendMessage(channelName,"Status: "+issue.getStatusObject().getName() );
                    event.getChannel().getBot().sendMessage(channelName,"Affects Version/s: "+issue.getAffectedVersions().toString() );
                    event.getChannel().getBot().sendMessage(channelName,"Fixes Version/s: "+issue.getFixVersions().toString() );
                    event.getChannel().getBot().sendMessage(channelName,"Assignee: "+issue.getAssignee().getDisplayName());
                    event.getChannel().getBot().sendMessage(channelName,"Labels: "+issue.getLabels().toString());
                } else {
                    event.getChannel().getBot().sendMessage(channelName,"Issue: issue "+issuekey+" does not exist" );
                }
            } else {
                event.getChannel().getBot().sendMessage(channelName,"Issue: Syntax error. You must provide an issue key to look for." );
                event.getChannel().getBot().sendMessage(channelName,"Use ?help issue for more information on the ?issue command syntax." );
            }
        }
    }



}
