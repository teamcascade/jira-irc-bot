/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.commands.jira;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.user.util.UserManager;
import org.teamcascade.plugins.jirabot.db.BotProject;
import org.teamcascade.plugins.jirabot.service.JiraService;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import java.util.ArrayList;
import java.util.List;

import org.teamcascade.plugins.jirabot.service.AoDataService;


public class CriticalCommand extends ListenerAdapter  {

    private AoDataService botData ;
    private JiraService jiraData;
    UserManager userManager = ComponentAccessor.getComponent(UserManager.class);

    public CriticalCommand(AoDataService data, JiraService jiraService) {
        this.botData = data;
        this.jiraData = jiraService;
    }

    @Override
    public void onMessage(MessageEvent event) throws SearchException {

        if (event.getMessage().equals("?critical")) {
            Boolean found = false;
            String channelName = event.getChannel().getName();
            User user = userManager.getUser("jirabot");

            ArrayList<BotProject> tracked = botData.getProjectsByChannel(channelName);
            if (tracked.size()>0) {
                for (int i = 0; i < tracked.size(); i++) {
                    BotProject project = tracked.get(i);
                    List<Issue> criticals =  jiraData.getCriticalIssuesByProject(user, project.getProjectID());
                    if (criticals.size()>0) {
                        event.getChannel().getBot().sendMessage(channelName,project.getKey()+"- "+criticals.size()+" critical issues were found:");

                        found = true;
                        for (int j=0; j<criticals.size(); j++) {
                            Issue issue = criticals.get(j);
                            event.getChannel().getBot().sendMessage(channelName,issue.getKey()+" - "+issue.getSummary());
                        }
                    }  else
                    {
                        event.getChannel().getBot().sendMessage(channelName,project.getKey()+": No critical issues found.");
                    }
                }
            } else {
                event.getChannel().getBot().sendMessage(channelName, "No project is being tracked in this channel. Please See ?projects for a list.");
                found = true;  // set to true even when not found so we dont have double message
            }
            if (!found) {
                event.getChannel().getBot().sendMessage(channelName,"No critical issues found for the tracked projects.");
            }
        }

    }

}