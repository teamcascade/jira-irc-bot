package org.teamcascade.plugins.jirabot.commands.bamboo;


import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.util.UserManager;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.teamcascade.plugins.jirabot.rest.bamboo.Result;
import org.teamcascade.plugins.jirabot.service.BambooService;
import org.teamcascade.plugins.jirabot.service.JiraService;

import java.util.Map;

public class LatestCommand extends ListenerAdapter {
    private JiraService jiraData;
    private BambooService bambooData;
    UserManager userManager = ComponentAccessor.getComponent(UserManager.class);

    public LatestCommand(BambooService bambooService, JiraService jiraService) {
        this.bambooData = bambooService;
        this.jiraData = jiraService;
    }

    public void onMessage(MessageEvent event) throws Exception {

        if (event.getMessage().startsWith("?latest")) {
            String channelName = event.getChannel().getName();
            String message = event.getMessage();
            if (message.length()>9) {
                String projectkey = message.substring(8);
                User user = userManager.getUser("jirabot");
                Project project =  jiraData.getProjectByKey(projectkey);
                Result result =  bambooData.getProjectLatestBuild(projectkey);
                if (result != null) {

                    event.getChannel().getBot().sendMessage(channelName,"Debug: result != null ");

                } else {
                    // No Applinks define for the project key
                    event.getChannel().getBot().sendMessage(channelName,"Latest: Could not find the Bamboo application link for Project "+projectkey );
                }
            } else {
                event.getChannel().getBot().sendMessage(channelName,"Latest: Syntax error. You must provide a project key to locate build results for." );
                event.getChannel().getBot().sendMessage(channelName,"Use ?help latest for more information on the ?latest command syntax." );
            }
        }
    }




}
