package org.teamcascade.plugins.jirabot.commands.jira;


import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.user.util.UserManager;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.teamcascade.plugins.jirabot.service.AoDataService;
import org.teamcascade.plugins.jirabot.service.JiraService;

import java.util.List;

public class SearchCommand extends ListenerAdapter {

    private AoDataService botData ;
    private JiraService jiraData;
    UserManager userManager = ComponentAccessor.getComponent(UserManager.class);

    public SearchCommand(AoDataService data, JiraService jiraService) {
        this.botData = data;
        this.jiraData = jiraService;
    }

    public void onMessage(MessageEvent event) throws Exception {
        //TODO: check SAL API text search, see of thats easier/better. These here can already be done manually with ?jql
        if (event.getMessage().startsWith("?search")) {
            String channelName = event.getChannel().getName();
            String message = event.getMessage();
            if (message.length()>8) {
                String text = message.substring(8);
                SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
                User user = userManager.getUser("jirabot");
                Boolean found = false;
                List<Issue> issues =  jiraData.textSearch(user, text);
                if (issues.size()>0) {
                    event.getChannel().getBot().sendMessage(channelName,"Text Search: Found "+issues.size()+" matching issues :");
                    found = true;
                    for (int j=0; j<issues.size(); j++) {
                        Issue issue = issues.get(j);
                        IssueLink path =  ComponentAccessor.getIssueLinkManager().getIssueLink(issue.getId());
                        //String url = jiraHelper.getRequest().getRequestURL().toString();
                        event.getChannel().getBot().sendMessage(channelName,"    "+issue.getKey()+": "+issue.getSummary()+" ("+issue.getStatusObject().getName()+")");
                    }
                }  else  {
                    event.getChannel().getBot().sendMessage(channelName,"Text Search: No matching issues found.");
                }
            } else {
                event.getChannel().getBot().sendMessage(channelName,"Search: Syntax error. You must provide text to search." );
                event.getChannel().getBot().sendMessage(channelName,"Use ?help search for more information on the ?search command syntax." );
            }
        }
    }

}
