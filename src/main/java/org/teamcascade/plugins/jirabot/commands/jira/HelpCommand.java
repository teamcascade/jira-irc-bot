package org.teamcascade.plugins.jirabot.commands.jira;


import com.sun.jersey.core.util.StringIgnoreCaseKeyComparator;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HelpCommand extends ListenerAdapter {
    Map<String,String> commands;
    public HelpCommand(Map<String,String> loadedCommands) {
        this.commands = loadedCommands;
    }

    public void onMessage(MessageEvent event) throws Exception {

        if (event.getMessage().startsWith("?help")) {
            String channelName = event.getChannel().getName();
            String message = event.getMessage();
            if (message.length()>6) {
               String commandname = message.substring(6);
               if (commands.containsKey(commandname)) {
                   event.getChannel().getBot().sendMessage(channelName,commands.get(commandname));
               }  else {
                   event.getChannel().getBot().sendMessage(channelName,"Sorry I cannot offer any help on command ?"+commandname+". Please type ?help for a list of all commands." );
               }
            }  else {
                StringBuilder builder = new StringBuilder();
                for (Map.Entry<String, String> entry : commands.entrySet())
                {
                    builder.append(entry.getKey()+", ");

                }
                String commandlist = builder.toString().substring(0,builder.toString().length()-1);
                event.getChannel().getBot().sendMessage(channelName,"Use ?help [command] for more information on any of the following commands:" );
                event.getChannel().getBot().sendMessage(channelName,commandlist);

            }

        }
    }
}
