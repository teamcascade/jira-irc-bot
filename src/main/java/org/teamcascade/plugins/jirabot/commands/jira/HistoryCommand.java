package org.teamcascade.plugins.jirabot.commands.jira;


import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.util.UserManager;
import org.ofbiz.core.entity.GenericValue;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.teamcascade.plugins.jirabot.service.AoDataService;
import org.teamcascade.plugins.jirabot.service.JiraService;

import java.util.List;


public class HistoryCommand extends ListenerAdapter {


    private AoDataService botData ;
    private JiraService jiraData;
    UserManager userManager = ComponentAccessor.getComponent(UserManager.class);

    public HistoryCommand(AoDataService data, JiraService jiraService) {
        this.botData = data;
        this.jiraData = jiraService;
    }

    public void onMessage(MessageEvent event) throws Exception {

        if (event.getMessage().startsWith("?history")) {
            String channelName = event.getChannel().getName();
            String message = event.getMessage();
            if (message.length()>9) {
                String issuekey = message.substring(9);
                User user = userManager.getUser("jirabot");
                Issue issue =  jiraData.getIssueByKey(issuekey);
                if (issue != null) {
                    List<GenericValue> history = jiraData.getWorkFlowHistoryByKey(issuekey);
                    if (history.isEmpty()) {
                        event.getChannel().getBot().sendMessage(channelName,"History: did not find any history for issue "+issuekey+"." );
                    } else {
                        event.getChannel().getBot().sendMessage(channelName,"History for "+issuekey+" / "+issue.getSummary()+": " );
                        for (int i = 0; i < history.size(); i++) {
                            GenericValue workflow =  history.get(i);
                            // TODO: Figure out the display later
                        }

                    }


                } else {
                    event.getChannel().getBot().sendMessage(channelName,"History: could not locate the issue requested." );
                }
            } else {
                event.getChannel().getBot().sendMessage(channelName,"History: Syntax error. You must provide an issue key to look for." );
                event.getChannel().getBot().sendMessage(channelName,"Use ?help history for more information on the ?history command syntax." );
            }
        }
    }


}
