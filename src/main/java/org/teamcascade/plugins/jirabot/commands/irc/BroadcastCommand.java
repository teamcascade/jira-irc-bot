package org.teamcascade.plugins.jirabot.commands.irc;

import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.teamcascade.plugins.jirabot.JIRABotImpl;
import org.teamcascade.plugins.jirabot.service.AoDataService;

import java.util.Iterator;
import java.util.List;

public class BroadcastCommand extends ListenerAdapter {
    private AoDataService botData ;

    public BroadcastCommand(AoDataService data) {
        this.botData = data;
    }

    public void onMessage(MessageEvent event) throws Exception {

        if (event.getMessage().startsWith("?broadcast")) {
            String channelName = event.getChannel().getName();
            String message = event.getMessage();
            if (message.length()>12) {
                String text = message.substring(11);
                List<String> channelList = botData.getChannelsList();
                for (int i = 0; i < channelList.size(); i++) {
                    String channel = channelList.get(i);
                    event.getChannel().getBot().sendMessage(channel,text);
                }
            } else {
                event.getChannel().getBot().sendMessage(channelName,"Broadcast: Syntax error. You must provide a message to broadcast." );
                event.getChannel().getBot().sendMessage(channelName,"Use ?help broadcast for more information on the ?broadcast command syntax." );
            }
        }
    }
}
