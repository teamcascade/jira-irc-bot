package org.teamcascade.plugins.jirabot;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.*;
import org.teamcascade.plugins.jirabot.service.AoDataService;

public final class PluginManager {

    private final PluginEventManager myPluginEventManager;

    private final String myModuleKey;

    private final Runnable myStop;

    private final AoDataService myData;



    private PluginManager(PluginEventManager pluginEventManager, String moduleKey, AoDataService dataService, Runnable stop) {

        myPluginEventManager = pluginEventManager;

        myModuleKey = moduleKey;

        myStop = stop;

        myData = dataService;

    }



    public static void install(PluginEventManager pluginEventManager, String moduleKey, AoDataService data, Runnable stop) {

        pluginEventManager.register(new PluginManager(pluginEventManager, moduleKey, data, stop));

    }



    public void stop() {

        myPluginEventManager.unregister(this);

        if (myStop != null) myStop.run();

    }



    @PluginEventListener
    public void onShutdown(PluginFrameworkShutdownEvent event) {

        stop();

    }



    @PluginEventListener
    public void onPluginDisabled(PluginDisabledEvent event) {

        if (event == null) return;

        stopIfMe(event.getPlugin());

    }



    @PluginEventListener
    public void onFrameworkRestarting(PluginFrameworkWarmRestartingEvent event) {

        stop();

    }



    @PluginEventListener
    public void onModuleDisabled(PluginModuleDisabledEvent event) {

        if (event == null) return;

        ModuleDescriptor module = event.getModule();

        if (module == null) return;

        Plugin plugin = module.getPlugin();

        if (plugin == null) return;
        String pluginKey = "org.teamcascade.plugins.jirabot";


        if (pluginKey.equals(plugin.getKey()) && myModuleKey.equals(module.getKey())) {

            stop();

        }

    }



    @PluginEventListener
    public void onPluginUninstalledEvent(PluginUninstalledEvent event) {

        if (event.getPlugin() == null) return;

        String pluginKey = "org.teamcascade.plugins.jirabot";

        if (pluginKey.equals(event.getPlugin().getKey())) {

            myData.Cleanup();

        }
    }



    @PluginEventListener
    public void onPluginRefreshedEvent(PluginRefreshedEvent event) {

        if (event == null) return;

        stopIfMe(event.getPlugin());

    }



    protected void stopIfMe(Plugin plugin) {

        if (plugin == null) return;
        String pluginKey = "org.teamcascade.plugins.jirabot";

        if (pluginKey.equals(plugin.getKey())) {

            stop();

        }

    }

}