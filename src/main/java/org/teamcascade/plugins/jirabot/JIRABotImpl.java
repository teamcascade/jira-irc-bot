/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot;

import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.plugin.event.events.PluginEnabledEvent;

import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;
import org.pircbotx.exception.NickAlreadyInUseException;

import org.apache.log4j.Logger;

import org.teamcascade.plugins.jirabot.commands.bamboo.LatestCommand;
import org.teamcascade.plugins.jirabot.commands.irc.BroadcastCommand;
import org.teamcascade.plugins.jirabot.commands.irc.BrokenCommand;
import org.teamcascade.plugins.jirabot.commands.jira.*;
import org.teamcascade.plugins.jirabot.service.AoDataService;
import org.teamcascade.plugins.jirabot.service.BambooService;
import org.teamcascade.plugins.jirabot.service.JiraService;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class JIRABotImpl implements JIRABot, LifecycleAware {

    // KEY and JOB_NAME are used byb the PluginScheduler to uniquely identify a job
    static final String KEY = JIRABotImpl.class.getName() + ":instance";
    private static final String JOB_NAME = JIRABotImpl.class.getName() + ":job";
    private final Logger log = Logger.getLogger(JIRABotImpl.class);
    private final EventPublisher eventPublisher;
    private final AoDataService botData ;
    private final JiraService jiraData;
    private final BambooService bambooData;
    private String botStatus = "Disconnected";
    private boolean dataLoaded = false;
    private Map<Long,String> issueTransitions = new HashMap<Long,String>();
    private static PircBotX IRCbot ;
    private Map<String,String> help = new HashMap<String, String>();





    public JIRABotImpl(EventPublisher eventPublisher,EntityLinkService entityLinkService, ActiveObjects ao) {

        // Load configuration information from Active Objects
        this.botData = new AoDataService(ao);
        this.eventPublisher = eventPublisher;
        this.IRCbot = new PircBotX();
        this.jiraData = new JiraService();
        this.bambooData = new BambooService(entityLinkService);

        // Add the transitions we want to track
        this.issueTransitions.put(EventType.ISSUE_CLOSED_ID,"CLOSING");
        this.issueTransitions.put(EventType.ISSUE_CREATED_ID,"CREATING");
        this.issueTransitions.put(EventType.ISSUE_DELETED_ID,"DELETING");
        this.issueTransitions.put(EventType.ISSUE_ASSIGNED_ID,"ASSIGNING");
        this.issueTransitions.put(EventType.ISSUE_COMMENTED_ID,"NEW COMMENT");
        this.issueTransitions.put(EventType.ISSUE_REOPENED_ID,"RE-OPENING");
        this.issueTransitions.put(EventType.ISSUE_RESOLVED_ID,"RESOLVED");
        this.issueTransitions.put(EventType.ISSUE_UPDATED_ID,"UPDATED");


    }

    public static PircBotX getIRCBot() {
        return IRCbot;
    }




    // LifecycleAware Interface

    public void onStart() {
        // register ourselves with the JIRA EventPublisher
        eventPublisher.register(this);

    }

    // Load Data and Connect to Server once the plugin has been enabled only

    @EventListener
    public void onPluginEnabledEvent(PluginEnabledEvent pluginEnabledEvent)  throws SQLException {

        if (!dataLoaded) {
            dataLoaded = botData.Initialize();
        }

        try {
            // connect the IRCBot
            connect();
            Boolean loadedall = true;
            //  Load the different listeners
            if (!IRCbot.getListenerManager().addListener(new ProjectsCommand(botData, jiraData))) {
                broadcastMessage("Warning: could not instantiate the ?projects command listener. ");
                loadedall=false;
            }
            if (!IRCbot.getListenerManager().addListener(new CriticalCommand(botData, jiraData))) {
                broadcastMessage("Warning: could not instantiate the ?critical command listener. ");
                loadedall=false;
            }
            if (!IRCbot.getListenerManager().addListener(new JQLCommand(botData, jiraData))) {
                broadcastMessage("Warning: could not instantiate ?jql command listener. ");
                loadedall=false;
            }
            if (!IRCbot.getListenerManager().addListener(new BrokenCommand())) {
                broadcastMessage("Warning: could not instantiate ?broken command listener. ");
                loadedall=false;
            }
            if (!IRCbot.getListenerManager().addListener(new SearchCommand(botData, jiraData))) {
                broadcastMessage("Warning: could not instantiate ?search command listener. ");
                loadedall=false;
            }
            if (!IRCbot.getListenerManager().addListener(new HistoryCommand(botData, jiraData))) {
                broadcastMessage("Warning: could not instantiate ?history command listener. ");
                loadedall=false;
            }
            if (!IRCbot.getListenerManager().addListener(new IssueCommand(botData, jiraData))) {
                broadcastMessage("Warning: could not instantiate ?issue command listener. ");
                loadedall=false;
            }

            if (!IRCbot.getListenerManager().addListener(new BroadcastCommand(botData))) {
                broadcastMessage("Warning: could not instantiate ?broadcast command listener. ");
                loadedall=false;
            }

            if (!IRCbot.getListenerManager().addListener(new LatestCommand(bambooData,jiraData))) {
                broadcastMessage("Warning: could not instantiate ?latest command listener. ");
                loadedall=false;
            }



            if (loadedall) {
                broadcastMessage("All ?commands registered successfully. Use ?help to learn more... ");
            }  else {
                broadcastMessage("Some ?commands were not registered properly. Use ?help to see the avi;able commands.");
            }

            // JIra Commands Package
            help.put("critical","The ?critical [project-key] command will show all issues with a Critical priority for a  project or all projects associated with this channel");
            help.put("jql","The ?jql [Query-String] command will show all issues that matches the JIRA Query Language string for projects associated with this channel");
            help.put("projects","The ?projects command will show the list of projects and their key that are associated with this channel");
            help.put("help","The ?help command will show the list of available commands. Use ?help [command name] to see help specific to that command.");
            help.put("search","The ?search command provides Textual search including wildcards (?,*). fuzzy logic (~) and relevance (^) ");
            help.put("history","The ?history [issue-key] command provides a workflow history for a specific issue. ");
            help.put("issue","The ?issue [issue-key] command provides mored detailed information about a specific issue.");
            help.put("broadcast","The ?broadcast [message] command will broadcast the message to all channels the Bot is currently in.");
            help.put("latest","The ?latest [project key] command will show the result of the latest build for that project.");

            if (!IRCbot.getListenerManager().addListener(new HelpCommand(help))) {
                broadcastMessage("Warning: could not instantiate ?help command listener. ");
            }


        } catch (NickAlreadyInUseException e) {
            log.error("NickAlreadyInUse when attempting to connect::" + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.error("IOException when attempting to connect::" + e.getMessage());
            e.printStackTrace();
        } catch (IrcException e) {
            log.error("IRC Exception when attempting to connect:" + e.getMessage());
            e.printStackTrace();
        }
    }

    @EventListener
    public void onPluginDisableEvent(PluginDisabledEvent pluginDisabledEvent) {
        IRCbot.disconnect();
        botData.Cleanup();
    }


    @Override
    public void connect() throws NickAlreadyInUseException, IOException, IrcException {

        final ArrayList<String> channels = botData.getChannelsList();
        final String nickName = botData.getNickName();
        final String serverURL =  botData.getServerUrl();

        // connect to IRC Network
        log.info("Attempting to connect to " + serverURL + " as  " + nickName);

        this.IRCbot.setName(nickName);
        this.IRCbot.connect(serverURL);

        // Join each channel associated with the Bot Instance
        for(int i=0; i<channels.size(); i++){
            this.IRCbot.joinChannel(channels.get(i));
        }


        log.info("Successfully connected to "+serverURL+" as  "+nickName);

    }
    @EventListener
    public void onIssueEvent(IssueEvent issueEvent) {
        Long eventTypeId = issueEvent.getEventTypeId();
        String action = issueTransitions.get(eventTypeId);
        if (action!= null) {
            Issue issue = issueEvent.getIssue();
            String projectKey = issueEvent.getProject().getKey();
            String projectDesc = issueEvent.getProject().getDescription();
            // Find all channels that have the project attached
            ArrayList<String> channels = botData.getChannelsListByProject(projectKey);
            for(int i=0; i<channels.size(); i++){
                String issueURL ;
                IRCbot.sendMessage(channels.get(i), "Project "+ projectKey + " / " + action +" "+issue.getKey()+" :"+issue.getSummary());

            }
        } else {
            log.error("Error: Unknown EventType ID");
        }
    }

    @Override
    public void broadcastMessage(String message) {
        for (Iterator iterator = IRCbot.getChannels().iterator(); iterator.hasNext(); ) {
            String channel = (String) iterator.next();
            IRCbot.sendMessage(channel,message);
        }
    }




}