/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.db;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.OneToMany;
import net.java.ao.OneToOne;
import net.java.ao.Preload;
import net.java.ao.schema.Table;                


@Preload
@Table("Bot")
public interface Bot extends Entity {

   // Accessors
	@Accessor("BotNickname")
    String getBotNickname();
    @Accessor("BotRealname")
    String getBotRealname();
    @Accessor("BotIdent")
    String getBotIdent();
    @Accessor("BotAltniik1")
    String getBotAltnick1();
    @Accessor("BotAltniik2")
    String getBotAltnick2();
    @Accessor("BotAltniik3")
    String getBotAltnick3();

    @Accessor("ServerName")
    String getServerName();
    @Accessor("ServerUrl")
    String getServerUrl();
    @Accessor("ServerPort")
    int getServerPort();
    @Accessor("UseSSL")
    boolean getUseSSL();
    @Accessor("Password")
    String getServerPassword();

    // Mutators
	@Mutator("BotNickname")
    void setBotNickname(String nickname);
    @Mutator("BotRealname")
    void setBotRealname(String realname);
    @Mutator("BotIdent")
    void setBotIdent(String ident);
    @Mutator("BotAltniik1")
    void setBotAltnick1(String altnick1);
    @Mutator("BotAltniik2")
    void setBotAltnick2(String altnick2);
    @Mutator("BotAltniik3")
    void setBotAltnick3(String altnick3);
    @Mutator("Connected")
    void setConnected(boolean connected);

    @Mutator("ServerName")
    void setServerName(String servername);
    @Mutator("ServerURL")
    void setServerUrl(String serverurl);
    @Mutator("ServerPort")
    void setServerPort(int serverPort);
    @Mutator("UseSSL")
    void setUseSSL(boolean useSSL);
    @Mutator("Password")
    void setServerPassword(String password);



    // Relationships
    @OneToMany
   	public BotChannel[] getChannels();   // Bot can monitor multiple channels at once
    @OneToMany
    public BotProject[] getProjects();   // Bot can monitor specific projects only

}
