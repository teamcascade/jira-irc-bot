/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.IssueRelationConstants;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.jql.builder.ConditionBuilder;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;


import java.net.URISyntaxException;
import java.net.URI;
import java.util.*;
import java.io.IOException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.teamcascade.plugins.jirabot.service.AoDataService;


public class JiraService {
    private URI jiraServerUri = UriBuilder.fromUri(ComponentAccessor.getApplicationProperties().getString("jira.baseurl")).build();
    private AoDataService botData;
    final ObjectMapper mapper = new ObjectMapper();
    final ProjectManager projectManager = ComponentAccessor.getProjectManager();
    final IssueManager issueManager = ComponentAccessor.getIssueManager();




    public  void JIRAClient(ActiveObjects ao)  throws URISyntaxException {
        this.botData = new AoDataService(ao);
    }

    public List<Issue> getCriticalIssues(User user) throws  SearchException {
        JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
        builder.where().priority().eq("Critical") ;
        Query query = builder.buildQuery();
        SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
        SearchResults results = searchService.search(user, query,PagerFilter.getUnlimitedFilter());
        return results.getIssues();
    }
    public List<Issue> getCriticalIssuesByProject(User user, Long projectID) throws  SearchException {
        JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
        builder.where().project().eq(projectID).and().priority().eq("Critical");
        Query query = builder.buildQuery();
        SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
        SearchResults results = searchService.search(user, query,PagerFilter.getUnlimitedFilter());
        return results.getIssues();
    }

    public List<Issue> jqlQuery(User user, String jqlQuery)  throws  SearchException {
        List<Issue> issues = null;
        SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
        SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlQuery);
        Query query = parseResult.getQuery();
        SearchResults results = searchService.search(user, query,PagerFilter.getUnlimitedFilter());
        return results.getIssues();

    }


    public List<Issue> textSearch(User user, String text) throws SearchException {
        String queryString = "summary ~ "+text+" OR description ~ "+text+" OR environment ~ "+ text;
        List<Issue> issues = null;
        SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
        SearchService.ParseResult parseResult = searchService.parseQuery(user, queryString);
        Query query = parseResult.getQuery();
        SearchResults results = searchService.search(user, query,PagerFilter.getUnlimitedFilter());

        return results.getIssues();

    }

    public Issue getIssueByKey(String issuekey) {
        Issue issue = issueManager.getIssueObject(issuekey);

        return issue;
    }

    public List<GenericValue> getWorkFlowHistoryByKey(String issuekey) throws GenericEntityException {
        Issue issue = issueManager.getIssueObject(issuekey);
        List<GenericValue> history =  issueManager.getEntitiesByIssueObject(IssueRelationConstants.WORKFLOW_HISTORY,issue);
        return history;

        /*
        This is a SQL Query that does the same trick?
        see https://confluence.atlassian.com/display/JIRACOM/Example+SQL+queries+for+JIRA#ExampleSQLqueriesforJIRA-GetWorklogdataforreporting

        select changegroup.author, changeitem.*
            from changeitem join changegroup on changegroup.id=changeitem.groupid
            join jiraissue on changegroup.issueid=jiraissue.id where
            jiraissue.pkey='GEN-3';
         */
    }

    public Project getProjectByKey(String projectkey) {
        Project project = projectManager.getProjectObjByKey(projectkey);
        return project;
    }
}