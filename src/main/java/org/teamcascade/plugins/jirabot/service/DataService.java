/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.service;

import java.sql.SQLException;
import java.util.ArrayList;

import org.teamcascade.plugins.jirabot.db.Bot;
import org.teamcascade.plugins.jirabot.db.BotChannel;
import org.teamcascade.plugins.jirabot.db.BotProject;

public interface DataService {

	/**
     * Gets the only Bot of the application. Creates one if it doesn't already exists.
     *
     * @return the Bot
     */	
	Bot getBot();


    /**
     * Add a project to the Bot list of projects. The Bot will report information on these projects only
     *
     * @return project instance
     */	

	BotProject addProject(String key, Long projectID);

    /**
     * Retrieve a project instance from the Bot list of projects.
     *
     * @return Channel instance
     */
    BotProject getProjectByKey(String projectkey);

    /**
     * Add a chat channel to the Bot list of channels. The Bot will try to connect to each channels
     *
     * @return Channel instance
     */

    BotChannel addChannel(String channelname);

	/**
     * Retrieve a chat channel instance from the Bot list of channels.  
     *
     * @return Channel instance
     */	
	BotChannel getChannel(int id);
	
	/**
     * Retrieve a chat channel instance from the Bot list of channels.  
     *
     * @return Channel instance
     */	
	BotChannel getChannelByName(String channelname);

    /**
     * Retrieve a collection of channels associated with a project
     *
     * @return Channel names collection
     */

    ArrayList<String> getChannelsListByProject(String projectkey);

    /**
     * Retrieve a collection of projects associated with a channel
     *
     * @return Project keys collection
     */

    ArrayList<String> getProjectsListByChannel(String channelname);

    /**
     * Retrieve a collection of projects associated with a channel
     *
     * @return Project keys collection
     */

    ArrayList<BotProject> getProjectsByChannel(String channelname);

    /**
     * Retrieve a collection of channels
     *
     * @return Channel collection
     */

    ArrayList<String> getChannelsList();

	/**
     * Retrieve a Project by ID
     *
     * @return Project instance
     */	
	BotProject getProject(int id);

    /**
     * Retrieve the server URL
     *
     * @return URL of IRC server
     */


    public boolean associateProjectToChannel(String channelname, String projectkey) throws SQLException;

    String getServerUrl();
    String getRealName();
    String getHostName();
    boolean isServerSSL();
    int getServerPort();
    String getNickName();
    String getAltNick1();
    String getAltNick2();
    String getAltNick3();
    String getIdent();
    String getServerPassword();



    public void setRealName(String p_realname);
    public void setNickName(String p_nickname);
    public void setIdent(String p_ident);
    public void setAltNick1(String p_nickname);
    public void setAltNick2(String p_nickname);
    public void setAltNick3(String p_nickname);
    public void setHostName(String hostname);
    public void setServerSSL(Boolean isSSL);
    public void setServerPort(int portNumber);
    public void setServerUrl(String url);
    public void setServerPassword(String password);


}
