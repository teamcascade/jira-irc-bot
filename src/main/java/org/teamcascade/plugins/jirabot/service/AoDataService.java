/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.plugins.jirabot.service;

import java.sql.SQLException;
import java.util.ArrayList;

import net.java.ao.Query;

import org.teamcascade.plugins.jirabot.db.*;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.tx.Transactional;


import static com.google.common.base.Preconditions.*;

@Transactional
public class AoDataService implements DataService {
	
    private final ActiveObjects ao;
    
    public AoDataService(ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }

    public boolean Initialize() throws SQLException{

        if (ao.find(Bot.class).length>0){
            return true;
        }

        // temporary fix: load data into active objects manually, till we get to doing the UI
        this.getBot();
        this.setHostName("Esper Net");
        this.setServerPort(6667);
        this.setServerUrl("irc.esper.net");
        this.setNickName("JIRABot");

        this.addChannel("#jirabot");
        this.addChannel("#teamcascade");
        this.addProject("TCB", 10201L);
        this.addProject("JIRABOT", 10300L);
        this.addProject("MES",10001L);
        this.addProject("WS",10002L);
        this.addProject("TCSUITE",10000L);
        this.associateProjectToChannel("JIRABOT", "#jirabot");
        this.associateProjectToChannel("TCSUITE", "#jirabot");
        this.associateProjectToChannel("TCB","#teamcascade");
        this.associateProjectToChannel("JIRABOT","#teamcascade");
        this.associateProjectToChannel("MES","#teamcascade");
        this.associateProjectToChannel("WS","#teamcascade");

        return true;
    }
    
    public void Cleanup() {
        // Cleanup Project_to_Channel objects
        final ProjectToChannel[] projectToChannels = ao.find(ProjectToChannel.class);
        for (int i = 0; i < projectToChannels.length; i++) {
            ProjectToChannel projectToChannel = projectToChannels[i];
            ao.delete(projectToChannel);
        }
        // Cleanup Bot_Project objects
        final BotProject[] projects = ao.find(BotProject.class);
        for (int i = 0; i < projects.length; i++) {
            BotProject project = projects[i];
            ao.delete(project);
        }
        // Cleanup Bot_Channel objects
        final BotChannel[] channels = ao.find(BotChannel.class);
        for (int i = 0; i < channels.length; i++) {
            BotChannel channel = channels[i];
            ao.delete(channel);
        }
        // Cleanup Bot_User objects
        final BotUser[] users = ao.find(BotUser.class);
        for (int i = 0; i < users.length; i++) {
            BotUser user = users[i];
            ao.delete(user);
        }

        // Cleanup Bot object
        final Bot[] bots = ao.find(Bot.class);
        for (int i = 0; i < bots.length; i++) {
            Bot bot = bots[i];
            ao.delete(bot);
        }


    }
	

	// Interface Methods
	@Override
	public Bot getBot() {
	       try
	        {
	            final Bot bot = getSingleBot();
	            return  (bot != null ? bot : createBot());
	        }
	        catch (SQLException e)
	        {
	            throw new RuntimeException(e);
	        }
	}
	
	@Override
	public BotChannel addChannel(String channelname) {
	    final BotChannel channel = ao.create(BotChannel.class);
	    
	    channel.setBot(getBot()); // setting the relationship
	    channel.setName(channelname);
	    channel.save();
	    return channel;
	}

    public BotChannel addChannel(String channelname, String topic) {
        final BotChannel channel = ao.create(BotChannel.class);

        channel.setBot(getBot()); // setting the relationship
        channel.setName(channelname);
        channel.setTopic(topic);
        channel.save();
        return channel;
    }



    // Internal Methods
	   private Bot getSingleBot() throws SQLException
	    {
	        final Bot[] bots = ao.find(Bot.class);

	        if (bots.length > 1)
	        {
	            throw new IllegalStateException("Application cannot have more than 1 IRC RoBot");
	        }

	        return bots.length > 0 ? bots[0] : null;
	    }

	    private Bot createBot() throws SQLException
	    {
	        final Bot bot = ao.create(Bot.class);
	        bot.save();
	        return bot;
	    }


		@Override
		public BotChannel getChannel(int id) {
			 return ao.get(BotChannel.class, id);
		}


		@Override
        public BotProject addProject(String key, Long projectID) {
            final BotProject project = ao.create(BotProject.class);

            project.setBot(getBot()); // setting the relationship
            project.setKey(key);
            project.setProjectID(projectID);
            project.save();
            return project;
        }


		@Override
		public BotProject getProject(int id) {
			 return ao.get(BotProject.class, id);
		}

		@Override
		public BotChannel getChannelByName(String channelname) {
			BotChannel channel;
			final BotChannel[] channels;
            channels = ao.find(BotChannel.class, Query.select().where("name = ?", channelname));
			// we assume the channels are unique for now...may need to add a check on creation?
			if (channels.length>0) {
				return channels[0];
			}
			return null;
		}

    @Override
    public ArrayList<String> getChannelsList() {
        final BotChannel[] botChannels = ao.find(BotChannel.class);

        ArrayList<String> channels = new ArrayList<String>();

        if (botChannels.length > 0) {
            for( BotChannel channel : botChannels ) {
              channels.add(channel.getName());
            }
        }
        return channels;

    }

    public ArrayList<String> getChannelsListByProject(String projectkey) {
       ArrayList<String> channels = new ArrayList<String>();
       ProjectToChannel[] relationship = ao.find(ProjectToChannel.class);     // Parsing the whole list to save SQL queries
        for (int i = 0; i < relationship.length; i++) {                       // Check efficiency with large list eventually
            ProjectToChannel projectToChannel = relationship[i];
            String key = projectToChannel.getProject().getKey();
            if (key.equals(projectkey)) {
                channels.add(projectToChannel.getChannel().getName());
            }
        }
        return channels;
    }


    public ArrayList<String> getProjectsListByChannel(String channelname) {
        ArrayList<String> projects = new ArrayList<String>();
        ProjectToChannel[] relationship = ao.find(ProjectToChannel.class);     // Parsing the whole list to save SQL queries
        for (int i = 0; i < relationship.length; i++) {                       // Check efficiency with large list eventually
            ProjectToChannel projectToChannel = relationship[i];
            String name = projectToChannel.getChannel().getName();
            if (name.equals(channelname)) {
                projects.add(projectToChannel.getProject().getKey());
            }
        }
        return projects;
    }

    public ArrayList<BotProject> getProjectsByChannel(String channelname) {
        ArrayList<BotProject> projects = new ArrayList<BotProject>();
        ProjectToChannel[] relationship = ao.find(ProjectToChannel.class);     // Parsing the whole list to save SQL queries
        for (int i = 0; i < relationship.length; i++) {                       // Check efficiency with large list eventually
            ProjectToChannel projectToChannel = relationship[i];
            String name = projectToChannel.getChannel().getName();
            if (name.equals(channelname)) {
                projects.add(projectToChannel.getProject());
            }
        }
        return projects;
    }


    public BotProject getProjectByKey(String projectkey) {
        final BotProject[] projects = ao.find(BotProject.class, Query.select().where("key = ?", projectkey));
        // we assume the projects are unique for now...may need to add a check on creation?
        if (projects.length > 0) {
            return projects[0];
        }
        return null;
    }

    @Override
    public boolean associateProjectToChannel(String projectkey, String channelname) throws SQLException
    {
        final BotChannel botChannel = getChannelByName(channelname);
        final BotProject botProject = getProjectByKey(projectkey);

        if (botChannel!=null && botProject!=null) {
          final ProjectToChannel projectToChannel = ao.create(ProjectToChannel.class);
          projectToChannel.setChannel(botChannel);
          projectToChannel.setProject(botProject);
          projectToChannel.save();
          return true;
        } else  {
          return false;
        }

    }


    public String getRealName() {
        return getBot().getBotRealname();
    }

    public String getHostName() {
        return getBot().getServerName();
    }

    public String getServerUrl() {
        return getBot().getServerUrl();
    }

    public boolean isServerSSL() {
        return getBot().getUseSSL();
    }

    public int getServerPort() {
        return getBot().getServerPort();
    }


    public String getNickName() {
        return getBot().getBotNickname();
    }

    public String getAltNick1() {
        return getBot().getBotAltnick1();
    }

    public String getAltNick2() {
        return getBot().getBotAltnick2();
    }

    public String getAltNick3() {
        return getBot().getBotAltnick3();
    }


    public String getIdent() {
        return getBot().getBotIdent();
    }


    public String getServerPassword() {
        return getBot().getServerPassword();
    }





    public void setRealName(String p_realname) {
        final Bot bot = getBot();
        bot.setBotRealname(p_realname);
        bot.save();
    }


    public void setNickName(String p_nickname) {
        final Bot bot = getBot();
        bot.setBotNickname(p_nickname);
        bot.save();
    }

    public void setIdent(String p_ident) {
        final Bot bot = getBot();
        bot.setBotIdent(p_ident);
        bot.save();
    }
    public void setAltNick1(String p_nickname) {
        final Bot bot = getBot();
        bot.setBotAltnick1(p_nickname);
        bot.save();
    }

    public void setAltNick2(String p_nickname) {
        final Bot bot = getBot();
        bot.setBotAltnick2(p_nickname);
        bot.save();
    }

    public void setAltNick3(String p_nickname) {
        final Bot bot = getBot();
        bot.setBotAltnick3(p_nickname);
        bot.save();
    }

    public void setHostName(String hostname) {
        final Bot bot = getBot();
        bot.setServerName(hostname);
        bot.save();
    }

    public void setServerSSL(Boolean isSSL) {
        final Bot bot = getBot();
        bot.setUseSSL(isSSL);
        bot.save();
    }

    public void setServerPort(int portNumber) {
        final Bot bot = getBot();
        bot.setServerPort(portNumber);
        bot.save();
    }

    public void setServerUrl(String url) {
        final Bot bot = getBot();
        bot.setServerUrl(url);
        bot.save();
    }

    public void setServerPassword(String password) {
        final Bot bot = getBot();
        bot.setServerPassword(password);
        bot.save();
    }

    public void setConnected(Boolean connected) {
        final Bot bot = getBot();
        bot.setConnected(connected);
        bot.save();  }


}
