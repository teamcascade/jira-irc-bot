/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package it.org.teamcascade.plugins.jirabot;


import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.auth.AnonymousAuthenticationHandler;
import com.atlassian.jira.rest.client.internal.jersey.JerseyJiraRestClientFactory;
import com.atlassian.sal.api.user.UserManager;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.DynamicJdbcConfiguration;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.teamcascade.plugins.jirabot.service.AoDataService;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(ActiveObjectsJUnitRunner.class)
@Jdbc(DynamicJdbcConfiguration.class)
public class RestClientTest {

    private EntityManager entityManager;
    private ActiveObjects ao;
    private AoDataService botData;

    private UserManager userManager;
    private JerseyJiraRestClientFactory factory;
    private AnonymousAuthenticationHandler anonymousAuthenticationHandler ;
    private URI jiraServerUri;
    private JiraRestClient restClient ;
    private NullProgressMonitor pm;
    private ClientConfig config;
    private Client client;
    private WebResource service;
    private ObjectMapper mapper;


    @Before
    public void setUp() throws Exception
    {
        factory = new JerseyJiraRestClientFactory();
        jiraServerUri  = UriBuilder.fromUri("http://localhost:8080/").build();
        anonymousAuthenticationHandler = new AnonymousAuthenticationHandler();
        restClient = factory.create(jiraServerUri,anonymousAuthenticationHandler);
        pm = new NullProgressMonitor();
        this.botData = new AoDataService(ao);
        config = new DefaultClientConfig();
        client = Client.create(config);
        service = client.resource(jiraServerUri);
        mapper = new ObjectMapper();
    }
}
