/*
 * This file is part of Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is licensed under the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * Unnamed - org.teamcascade.plugins:jirabot:atlassian-plugin:1.0-SNAPSHOT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package ut.org.teamcascade.plugins.jirabot;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.sal.api.user.UserManager;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.*;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.teamcascade.plugins.jirabot.db.*;
import org.teamcascade.plugins.jirabot.service.AoDataService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(ActiveObjectsJUnitRunner.class)
@Jdbc(DynamicJdbcConfiguration.class)
public class AoDataServiceTest {

    private EntityManager entityManager;
    private ActiveObjects ao;
    private AoDataService dataService;
    private UserManager userManager;


    @Before
    public void setUp() throws Exception
    {
        assertNotNull(entityManager);
        ao = new TestActiveObjects(entityManager);
        dataService = new AoDataService(ao);
        entityManager.migrate(Bot.class);
        entityManager.migrate(BotProject.class);
        entityManager.migrate(BotChannel.class);
        entityManager.migrate(BotUser.class);
        entityManager.migrate(ProjectToChannel.class);


        // Mocking //
        userManager = mock(UserManager.class);
        when(userManager.getRemoteUsername()).thenReturn("admin");
    }




    @Test
    public void testGetBot() throws Exception
    {
        assertEquals(0, entityManager.find(Bot.class).length); // no bot to start with
        assertNotNull(dataService.getBot());                   // getBot should create the bot as needed
        entityManager.flushAll();
    }

    @Test
    public void testAddChannel() throws Exception {
        BotChannel channel;
        assertEquals(0, entityManager.find(BotChannel.class).length); // no channels should exist
        assertNotNull(dataService.getBot());                          // getBot should create the bot as needed
        channel = dataService.addChannel("#teamcascade");             // add a channel
        assertEquals(1, entityManager.find(BotChannel.class).length); // should have 1 and only 1 channel
        assertEquals("#teamcascade", channel.getName());              // Channel name should be set


    }



    @Test
    public void testGetChannelByName() throws Exception {
        BotChannel channel1;
        BotChannel channel2;
        String channelname1 = "#teamcascade";
        String channelname2 = "#jirabot";
        assertEquals(0, entityManager.find(BotChannel.class).length); // no channels should exist
        channel1 = dataService.addChannel(channelname1);               // add a channel
        channel2 = dataService.addChannel(channelname2);               // add a second channel
        assertEquals("Channel 1 should be returned",channel1,dataService.getChannelByName(channelname1));
        assertEquals("Channel 2 should be returned",channel2,dataService.getChannelByName(channelname2));
        assertNull("Should be Null when the channel does not exist",dataService.getChannelByName("randomname"));
    }

    @Test
    public void testGetChannelByID() throws Exception {
        Integer id1;
        Integer id2;
        assertEquals(0, entityManager.find(BotChannel.class).length); // no channels should exist
        BotChannel channel1 = dataService.addChannel("#teamcascade");
        BotChannel channel2 = dataService.addChannel("#jirabot");
        id1 = channel1.getID();
        id2 = channel2.getID();
        assertEquals(2, entityManager.find(BotChannel.class).length); // Should be 2 channels now
        assertEquals(channel1, dataService.getChannel(id1));          // Should be the same when requested by ID
        assertNotSame(channel2, dataService.getChannel(id1));         // Should be different with wrong ID
        assertEquals(channel2, dataService.getChannel(id2));          // Should be the same when requested by ID
        assertNull("Should be Null when the ID doesnt exist",dataService.getChannel(99));
    }

    @Test
    public void testGetChannelsList() throws  Exception {
        ArrayList<String> channels;
        assertEquals(0, entityManager.find(BotChannel.class).length); // no channels should exist
        assertEquals("Size should be zero when no channels exist",dataService.getChannelsList().size(),0);
        BotChannel channel1 = dataService.addChannel("#teamcascade");
        BotChannel channel2 = dataService.addChannel("#jirabot");
        channels = dataService.getChannelsList();
        assertEquals("Array size should be 2 after",2, channels.size());
        assertEquals("Array should have #teamcascade in 0",channels.get(0),"#teamcascade");
        assertEquals("Array should have #jirabot in 1",channels.get(1),"#jirabot");

    }

    @Test
    public void testGetChannelsByProject() throws Exception  {
        BotChannel channel1;
        BotChannel channel2;
        String channelname1 = "#teamcascade";
        String channelname2 = "#jirabot";
        assertEquals(0, entityManager.find(BotChannel.class).length); // no channels should exist
        channel1 = dataService.addChannel(channelname1);               // add a channel
        channel2 = dataService.addChannel(channelname2);               // add a second channel
        BotProject project1;
        BotProject project2;
        String projectkey1 = "DEMO1";
        String projectkey2 = "DEMO2";
        assertEquals(0, entityManager.find(BotProject.class).length); // no projects should exist
        project1 = dataService.addProject(projectkey1,10000L);               // add a project
        project2 = dataService.addProject(projectkey2,10001L);               // add a project


    }

    @Test
    public void testAddProject() throws Exception {
        BotProject project;
        assertEquals(0, entityManager.find(BotProject.class).length); // no projects should exist
        assertNotNull(dataService.getBot());                          // getBot should create the bot as needed
        project = dataService.addProject("DEMO",10000L);                     // add a project
        assertEquals(1, entityManager.find(BotProject.class).length); // should have 1 and only 1 project
        assertEquals("DEMO", project.getKey());                      // project key should be set
    }

    @Test
    public void testGetProjectByKey() throws Exception {
        BotProject project1;
        BotProject project2;
        String projectkey1 = "DEMO1";
        String projectkey2 = "DEMO2";
        assertEquals(0, entityManager.find(BotProject.class).length); // no projects should exist
        project1 = dataService.addProject(projectkey1,10000L);               // add a project
        project2 = dataService.addProject(projectkey2,10001L);               // add a project
        assertEquals("Project 1 should be returned",project1,dataService.getProjectByKey(projectkey1));
        assertEquals("Project 2 should be returned",project2,dataService.getProjectByKey(projectkey2));
        assertNull("Unknown projectkey should return Null",dataService.getProjectByKey("unknown"));

    }

    @Test
    public void testGetProjectByID() throws Exception {
        Integer id1;
        Integer id2;
        assertEquals(0, entityManager.find(BotProject.class).length); // no projects should exist
        BotProject project1 = dataService.addProject("DEMO1",10000L);
        BotProject project2 = dataService.addProject("DEMO2",10000L);
        id1 = project1.getID();
        id2 = project2.getID();
        assertEquals(2, entityManager.find(BotProject.class).length); // 2 projects should exists now
        assertEquals(project1, dataService.getProject(id1));          // Should be the same when requested by ID
        assertNotSame(project2, dataService.getProject(id1));         // Should be different with wrong ID
        assertEquals(project2, dataService.getProject(id2));          // Should be the same when requested by ID
        assertNull("Should be Null when ID doesnt exist",dataService.getProject(99));

    }

    @Test
    public void testAssociateProjectToChannel() throws Exception {
        BotChannel channel1;
        BotChannel channel2;
        String channelname1 = "#teamcascade";
        String channelname2 = "#jirabot";
        assertEquals(0, entityManager.find(BotChannel.class).length); // no channels should exist
        channel1 = dataService.addChannel(channelname1);               // add a channel
        channel2 = dataService.addChannel(channelname2);               // add a second channel
        BotProject project1;
        BotProject project2;
        String projectkey1 = "DEMO1";
        String projectkey2 = "DEMO2";
        assertEquals(0, entityManager.find(BotProject.class).length); // no projects should exist
        assertFalse("Associating Project1 and Channel1 should fail without a project1",dataService.associateProjectToChannel(projectkey1,channelname1));
        project1 = dataService.addProject(projectkey1,10000L);               // add a project
        project2 = dataService.addProject(projectkey2,10001L);               // add a project
        assertFalse("Associating Project1 and ChannelX should fail without a ChannelX",dataService.associateProjectToChannel(projectkey1,"#ducktalk"));
        assertEquals(0, entityManager.find(ProjectToChannel.class).length); // no relationship should exist yet
        assertTrue("Associating Project1 and Channel1",dataService.associateProjectToChannel(projectkey1,channelname1));
        assertTrue("Associating Project1 and Channel2",dataService.associateProjectToChannel(projectkey1,channelname2));
        assertTrue("Associating Project2 and Channel2",dataService.associateProjectToChannel(projectkey2,channelname2));

    }

    @Test
    public void getChannelsListByProject() throws  Exception {
        ArrayList<String> channelList = new ArrayList<String>();
        BotChannel channel1;
        BotChannel channel2;
        String channelname1 = "#teamcascade";
        String channelname2 = "#jirabot";
        channel1 = dataService.addChannel(channelname1);               // add a channel
        channel2 = dataService.addChannel(channelname2);               // add a second channel
        BotProject project1;
        BotProject project2;
        String projectkey1 = "DEMO1";
        String projectkey2 = "DEMO2";
        project1 = dataService.addProject(projectkey1,10000L);               // add a project
        project2 = dataService.addProject(projectkey2,10001L);               // add a project
        assertTrue("Associating Project1 and Channel1",dataService.associateProjectToChannel(projectkey1,channelname1));
        assertTrue("Associating Project1 and Channel2",dataService.associateProjectToChannel(projectkey1,channelname2));
        assertTrue("Associating Project2 and Channel2",dataService.associateProjectToChannel(projectkey2,channelname2));
        channelList = dataService.getChannelsListByProject("DEMO1");
        assertEquals("Should return 2 channels for project 1",2,channelList.size());
        assertEquals("Should return 2 channels for project 1",2,channelList.size());
        channelList = dataService.getChannelsListByProject("DEMO2");
        assertEquals("Should return 1 channels for project 2",channelList.size(),1);

    }

    @Test
    public void getProjectsListByChannel() throws  Exception {
        ArrayList<String> projectList = new ArrayList<String>();
        BotChannel channel1;
        BotChannel channel2;
        String channelname1 = "#teamcascade";
        String channelname2 = "#jirabot";
        channel1 = dataService.addChannel(channelname1);               // add a channel
        channel2 = dataService.addChannel(channelname2);               // add a second channel
        BotProject project1;
        BotProject project2;
        String projectkey1 = "DEMO1";
        String projectkey2 = "DEMO2";
        project1 = dataService.addProject(projectkey1,10000L);               // add a project
        project2 = dataService.addProject(projectkey2,10001L);               // add a project
        assertTrue("Associating Project1 and Channel1",dataService.associateProjectToChannel(projectkey1,channelname1));
        assertTrue("Associating Project1 and Channel2",dataService.associateProjectToChannel(projectkey1,channelname2));
        assertTrue("Associating Project2 and Channel2",dataService.associateProjectToChannel(projectkey2,channelname2));
        projectList = dataService.getProjectsListByChannel("#teamcascade");
        assertEquals("Should return 1 project for channel 1",1,projectList.size());
        projectList = dataService.getProjectsListByChannel("#jirabot");
        assertEquals("Should return 2 projects for Channel 2",projectList.size(),2);
}

    @Test
    public void testGetSetRealName() {
        assertNull("Realname is null before setter", dataService.getRealName());
        dataService.setRealName("JIRABot");
        assertNotNull("Realname is not null after setter",dataService.getRealName());
        assertEquals("Realname equals the set value after setter",dataService.getRealName(),"JIRABot");
    }

    @Test
    public void testGetSetHostName() {
        assertNull("Hostname is null before setter",dataService.getHostName());
        dataService.setHostName("irc.esper.net");
        assertNotNull("Hostname is not null after setter",dataService.getHostName());
        assertEquals("Hostname equals the set value after setter",dataService.getHostName(),"irc.esper.net");
    }


    @Test
    public void testGetSetServerURL() {
        assertNull("Server URL is should be empty before setter",dataService.getServerUrl());
        dataService.setServerUrl("irc.esper.net");
        assertEquals("Server URL should equals the set value after setter", dataService.getServerUrl(), "irc.esper.net");

    }

    @Test
    public void testGetSetServerSSL() {
        assertFalse("Server SSL is false before setter", dataService.isServerSSL());
        dataService.setServerSSL(true);
        assertEquals("ServerSLL equals the set value after setter",dataService.isServerSSL(),true);
    }



    @Test
    public void testGetSetServerPort() {
        assertEquals("Port Nunber is should be zero before setter",dataService.getServerPort(),0);
        dataService.setServerPort(6667);
        assertEquals("Port Number should equals the set value after setter",dataService.getServerPort(),6667);

    }

    @Test
    public void testGetSetNickName() {
        assertNull("Nickname should be null before setter",dataService.getNickName());
        dataService.setNickName("JIRABot");
        assertNotNull("Nickname is not null after setter",dataService.getNickName());
        assertEquals("Nickname equals the set value after setter",dataService.getNickName(),"JIRABot");
    }

    @Test
    public void testGetSetAltNick1() {
        assertNull("AltNick1 should be null before setter",dataService.getAltNick1());
        dataService.setAltNick1("JIRABot1");
        assertNotNull("AltNick1 is not null after setter",dataService.getAltNick1());
        assertEquals("AltNick1 equals the set value after setter",dataService.getAltNick1(),"JIRABot1");
    }
    @Test
    public void testGetSetAltNick2() {
        assertNull("AltNick2 should be null before setter",dataService.getAltNick2());
        dataService.setAltNick2("JIRABot2");
        assertNotNull("AltNick2 is not null after setter",dataService.getAltNick2());
        assertEquals("AltNick2 equals the set value after setter",dataService.getAltNick2(),"JIRABot2");
    }
    @Test
    public void testGetSetAltNick3() {
        assertNull("AltNick3 should be null before setter",dataService.getAltNick3());
        dataService.setAltNick3("JIRABot3");
        assertNotNull("AltNick3 is not null after setter",dataService.getAltNick3());
        assertEquals("AltNick3 equals the set value after setter",dataService.getAltNick3(),"JIRABot3");
    }

    @Test
    public void testGetSetIdent() {
        assertNull("Ident is null before setter",dataService.getIdent());
        dataService.setIdent("identity");
        assertNotNull("Ident is not null after setter",dataService.getIdent());
        assertEquals("Ident equals the set value after setter",dataService.getIdent(),"identity");
    }

    @Test
    public void testGetSetServerPassword() {
        assertNull("Password is null before setter",dataService.getServerPassword());
        dataService.setServerPassword("mypassword");
        assertNotNull("Password is not null after setter",dataService.getServerPassword());
        assertEquals("Password equals the set value after setter",dataService.getServerPassword(),"mypassword");
    }



}
